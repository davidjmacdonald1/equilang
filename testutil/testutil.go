package testutil

import "testing"

func Fail(t *testing.T, reason string, want, got any) {
	t.Helper()
	t.Fatalf("%s \nwant=%q\n got=%q", reason, want, got)
}
