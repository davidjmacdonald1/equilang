package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Println("Equilang")
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Print(">> ")
		scanned := scanner.Scan()
		if !scanned {
			return
		}
		line := scanner.Text()

		fmt.Println("You said:", line)
	}
}
