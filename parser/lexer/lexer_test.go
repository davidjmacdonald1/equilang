package lexer

import (
	"testing"

	"gitlab.com/davidjmacdonald1/equilang/parser/token"
	"gitlab.com/davidjmacdonald1/equilang/testutil"
)

func TestTokenize(t *testing.T) {
	tests := []lexerTest{
		makeTest(
			"cosx",
			token.Make("c", token.KindId),
			token.Make("o", token.KindId),
			token.Make("s", token.KindId),
			token.Make("x", token.KindId),
		),
	}

	for _, test := range tests {
		test.run(t)
	}
}

type lexerTest struct {
	Input  string
	Output []token.Token
}

func makeTest(input string, output ...token.Token) lexerTest {
	return lexerTest{Input: input, Output: output}
}

func (test *lexerTest) run(t *testing.T) {
	t.Helper()

	actuals, err := Tokenize(test.Input)
	if err != nil {
		t.Fatal("lexer error:", err)
	}

	expecteds := test.Output
	if len(expecteds) != len(actuals) {
		testutil.Fail(t, "wrong token length", expecteds, actuals)
	}

	for i, actual := range actuals {
		expected := expecteds[i]
		if actual.Text != expected.Text {
			testutil.Fail(t, "wrong token text", expected, actual)
		}

		if actual.Kind != expected.Kind {
			testutil.Fail(t, "wrong token kind", expected, actual)
		}
	}
}
