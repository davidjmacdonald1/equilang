package token

type Kind string

const (
	KindNum Kind = "Num"
	KindId       = "Id"
)

type Token struct {
	Text string
	Kind Kind
}

func Make(text string, kind Kind) Token {
	return Token{Text: text, Kind: kind}
}
